// Copyright 2019 Cashero.com
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package conf

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"sync"
	"time"
)

var (
	configPath     = "."
	configFileName = "conf.json"
)

type GbeConfig struct {
	BaseURL          string                 `json:"baseURL"`
	SiteBaseURL      string                 `json:"siteBaseURL"`
	MySQL            MySQLConfig            `json:"dataSource"`
	JwtSecret        string                 `json:"jwtSecret"`
	RestServer       RestServerConfig       `json:"restServer"`
	SecretMainServer SecretMainServerConfig `json:"secretMainServer"`
	LogLevel         string                 `json:"logLevel"`
	LogEnvironment   string                 `json:"logEnvironment"`
	SendGrid         SendGridConfig         `json:"sendGrid"`
	WebURL           string                 `json:"webUrl"`
	OAuth            OAuthConfig            `json:"oAuth"`
}

type OAuthConfig struct {
	ClientID     string `json:"clientID"`
	ClientSecret string `json:"clientSecret"`
}

type SendGridConfig struct {
	ApiKey      string `json:"sendGridKey"`
	SenderName  string `json:"senderName"`
	SenderEmail string `json:"senderEmail"`
}

type MySQLConfig struct {
	DriverName        string `json:"driverName"`
	Addr              string `json:"addr"`
	Database          string `json:"database"`
	User              string `json:"user"`
	Password          string `json:"password"`
	EnableAutoMigrate bool   `json:"enableAutoMigrate"`
	Retries           int    `json:"retries"`
	Port              uint16 `json:"port"`
}

type SecretMainServerConfig struct {
	Addr string `json:"addr"`
}

type RestServerConfig struct {
	Addr string `json:"addr"`
}

type ServerConfig struct {
	ServerMinTime time.Duration
	ServerTime    time.Duration
	ServerTimeout time.Duration
}

var config GbeConfig
var configOnce sync.Once

func SetConfFilePath(path string) {
	configPath = path
}

func SetConfFileName(name string) {
	configFileName = name
}

func GetConfig() *GbeConfig {
	configOnce.Do(func() {
		bytes, err := ioutil.ReadFile(configPath + "/" + configFileName)
		log.Println(configPath + "/" + configFileName)
		if err != nil {
			panic(err)
		}

		err = json.Unmarshal(bytes, &config)
		if err != nil {
			panic(err)
		}
	})
	return &config
}
