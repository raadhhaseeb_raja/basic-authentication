package rest

import (
	"github.com/GoChallenge/GoChallengeAuth/conf"
	"github.com/GoChallenge/GoChallengeAuth/logger"
	"github.com/GoChallenge/GoChallengeAuth/service"
	"github.com/GoChallenge/GoChallengeAuth/web/goTemplates"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"log"
)

func StartServer(container *service.Container) *HttpServer {
	// Inject services instance from ServiceContainer

	googleOauthConfig := &oauth2.Config{
		ClientID:     conf.GetConfig().OAuth.ClientID,
		ClientSecret: conf.GetConfig().OAuth.ClientSecret,
		Endpoint:     google.Endpoint,
		RedirectURL:  "http://localhost:8010/callback",
		Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
	}
	log.Println(googleOauthConfig)
	// Initializing Controllers
	userController := NewUserController(container.JWTService, container.Mailer, *googleOauthConfig, container.UserService)
	templateController := goTemplates.NewTemplateController(container.JWTService, container.UserService)

	//Initializing Middleware
	middleWare := NewMiddleWare(container.UserService, container.JWTService)

	//Initializing http server
	httpServer := NewHttpServer(
		container.GbeConfigService.GetConfig().RestServer.Addr,
	)

	//Inject controller instance to server
	httpServer.userController = userController
	httpServer.middleWare = middleWare
	httpServer.templateController = templateController
	go httpServer.Start()
	logger.Instance().Info("rest server ok")
	return httpServer
}
