package rest

import (
	"encoding/json"
	"github.com/GoChallenge/GoChallengeAuth/models"
	"github.com/GoChallenge/GoChallengeAuth/service"
	"github.com/rs/xid"
	"golang.org/x/oauth2"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type UserController interface {
	InitForgotPassword(w http.ResponseWriter, r *http.Request)
	UpdatePassword(w http.ResponseWriter, r *http.Request)
	Login(w http.ResponseWriter, r *http.Request)
	Signup(w http.ResponseWriter, r *http.Request)
	UpdateProfile(w http.ResponseWriter, r *http.Request)
	GetProfile(w http.ResponseWriter, r *http.Request)
	CreateProfile(w http.ResponseWriter, r *http.Request)
	HandleGoogleLogin(w http.ResponseWriter, r *http.Request)
	HandleGoogleCallback(w http.ResponseWriter, r *http.Request)
}

type userController struct {
	jwtService    service.JWTService
	userService   service.UserService
	mailerService service.Mailer
	oauthConfig   oauth2.Config
}

func NewUserController(jwtService service.JWTService, mailer service.Mailer, oauthConf oauth2.Config, userService service.UserService) UserController {
	return &userController{

		jwtService:    jwtService,
		mailerService: mailer,
		userService:   userService,
		oauthConfig:   oauthConf,
	}
}

func (u userController) Signup(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	bodyBytes, e := ioutil.ReadAll(r.Body)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}
	var req models.UserRequest
	e = json.Unmarshal(bodyBytes, &req)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}

	user := &models.User{
		UID:      xid.New().String(),
		Email:    req.Email,
		Password: req.Password,
	}

	sr := u.userService.Signup(*user)
	if sr != nil {
		WriteErrorJson(w, &sr, http.StatusOK)
		return
	}

	WriteJson(w, models.NewStandardResponse(true, 1000, "Success", *user), http.StatusOK)
}

func (u userController) Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	bodyBytes, e := ioutil.ReadAll(r.Body)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}
	var req models.UserRequest
	e = json.Unmarshal(bodyBytes, &req)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}

	user := &models.User{
		Email:    req.Email,
		Password: req.Password,
	}
	resp, err := u.userService.Login(user)
	log.Println(resp, err)
	if err != nil {
		WriteErrorJson(w, &err, http.StatusOK)
		return
	}

	log.Println(resp)

	getToken, er := u.jwtService.GenerateToken(resp.Email, resp.UID)
	if er != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}
	log.Println(getToken)
	responseVo := GetLoginResponseVo(resp.Email, resp.UID, resp.Password, getToken)
	WriteJson(w, models.NewStandardResponse(true, 1000, "Success", responseVo), http.StatusOK)
}

func (u userController) InitForgotPassword(w http.ResponseWriter, r *http.Request) {
	bodyBytes, e := ioutil.ReadAll(r.Body)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}

	var req models.ForgetPasscode
	e = json.Unmarshal(bodyBytes, &req)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}

	token, resp := u.userService.ForgotPassword(req, u.jwtService)
	if resp != nil {
		WriteErrorJson(w, resp, http.StatusOK)
		return
	}
	WriteJson(w, models.NewStandardResponse(true, 1000, "Success", token), http.StatusOK)
}

func (u userController) UpdateProfile(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	user := getCurrentUser(r.Context())
	if len(strings.TrimSpace(user)) == 0 {
		WriteErrorJson(w, models.NewStandardResponse(false, 403, "Invalid Token", nil), http.StatusOK)
		return
	}
	if len(strings.TrimSpace(user)) == 0 {
		WriteErrorJson(w, models.NewStandardResponse(false, 403, "Invalid Token", nil), http.StatusOK)
		return
	}
	bodyBytes, e := ioutil.ReadAll(r.Body)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}
	var req models.Profile
	e = json.Unmarshal(bodyBytes, &req)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}
	req.UID = user
	updatedProfile, sr := u.userService.UpdateProfile(&req)
	if sr != nil {
		WriteErrorJson(w, sr, http.StatusOK)
		return
	}
	WriteJson(w, models.NewStandardResponse(true, 1000, "Success", updatedProfile), http.StatusOK)
}

func (u userController) GetProfile(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	user := getCurrentUser(r.Context())
	if len(strings.TrimSpace(user)) == 0 {
		WriteErrorJson(w, models.NewStandardResponse(false, 403, "Invalid Token", nil), http.StatusOK)
		return
	}
	profile, sr := u.userService.GetProfile(user)
	if sr != nil {
		WriteErrorJson(w, sr, http.StatusOK)
		return
	}
	WriteJson(w, models.NewStandardResponse(true, 1000, "Success", profile), http.StatusOK)
}

func (u userController) UpdatePassword(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")

	bodyBytes, e := ioutil.ReadAll(r.Body)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}

	var req models.UpdatePasswordRequest
	e = json.Unmarshal(bodyBytes, &req)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusBadRequest, "Bad Request", nil), http.StatusOK)
		return
	}
	if req.Password != req.ConfirmPassword {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusBadRequest, "Incorrect Passwords", nil), http.StatusOK)
		return
	}
	currentuser := getUserData(r.Context())
	log.Println(currentuser)
	if currentuser == nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusBadRequest, "Invalid Token", nil), http.StatusOK)
		return
	}

	user, resp := u.userService.UpdatePassword(currentuser, &req)
	if resp != nil {
		WriteErrorJson(w, resp, http.StatusOK)
		return
	}

	getToken, er := u.jwtService.GenerateToken(user.Email, user.UID)
	if er != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}
	responseVo := GetLoginResponseVo(user.Email, user.UID, user.Password, getToken)

	WriteJson(w, models.NewStandardResponse(true, 1000, "Success", responseVo), http.StatusOK)
}

func (u userController) CreateProfile(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	user := getCurrentUser(r.Context())
	if len(strings.TrimSpace(user)) == 0 {
		WriteErrorJson(w, models.NewStandardResponse(false, 403, "Invalid Token", nil), http.StatusOK)
		return
	}
	bodyBytes, e := ioutil.ReadAll(r.Body)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}

	var req models.Profile
	e = json.Unmarshal(bodyBytes, &req)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusBadRequest, "Bad Request", nil), http.StatusOK)
		return
	}
	req.UID = user
	profile, sr := u.userService.CreateProfile(req)
	if sr != nil {
		WriteErrorJson(w, sr, http.StatusOK)
		return
	}
	WriteJson(w, models.NewStandardResponse(true, 1000, "Success", profile), http.StatusOK)
}

func (u userController) HandleGoogleLogin(w http.ResponseWriter, r *http.Request) {
	url := u.oauthConfig.AuthCodeURL("some state")
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func (u userController) HandleGoogleCallback(w http.ResponseWriter, r *http.Request) {
	if r.FormValue("state") != "some state" {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	}

	token, err := u.oauthConfig.Exchange(oauth2.NoContext, r.FormValue("code"))
	if err != nil {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	resp, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	var googleAuthUser models.GoogleAuthSignIn

	e := json.Unmarshal(content, &googleAuthUser)
	if e != nil {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}

	if len(strings.TrimSpace(googleAuthUser.Email)) == 0 {
		WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
		return
	}

	sr := u.userService.LoginWithGoogle(&googleAuthUser)
	if sr != nil {
		if strings.Contains(sr.Message, "already registered via email signup") {
			http.Redirect(w, r, "/signup", http.StatusPermanentRedirect)
			return
		}
		if strings.Contains(sr.Message, "User Already Exists") || strings.Contains(sr.Message, "record not found") {
			getToken, er := u.jwtService.GenerateToken(googleAuthUser.Email, googleAuthUser.ID)
			if er != nil {
				WriteErrorJson(w, models.NewStandardResponse(false, http.StatusInternalServerError, "Internal Server Error", nil), http.StatusOK)
				return
			}
			log.Println(getToken)
			http.Redirect(w, r, "/redirect?token="+getToken, http.StatusTemporaryRedirect)
			return
		} else {
			// do something
		}
	}
}
