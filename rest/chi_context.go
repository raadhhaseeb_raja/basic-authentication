package rest

import (
	"context"
	"github.com/GoChallenge/GoChallengeAuth/models"
)

func getCurrentUser(ctx context.Context) string {
	v, ok := ctx.Value(models.KeyCurrentUser).(string)
	if !ok {
		return ""
	}
	return v
}

func getUserData(ctx context.Context) *models.HasForgottenPassword {
	val := ctx.Value(models.KeyCurrentContext)
	return val.(*models.HasForgottenPassword)
}
