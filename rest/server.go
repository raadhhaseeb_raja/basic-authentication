package rest

import (
	"github.com/GoChallenge/GoChallengeAuth/web/goTemplates"
	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"net/http"
	"time"
)

type HttpServer struct {
	addr               string
	middleWare         MiddleWare
	userController     UserController
	templateController goTemplates.TemplateController
}

//NewHttpServer create server instance
func NewHttpServer(addr string,
) *HttpServer {
	return &HttpServer{
		addr: addr,
	}
}
func (s HttpServer) GetTemplateController() goTemplates.TemplateController {
	return s.templateController
}

func (s HttpServer) GetUserController() UserController {
	return s.userController
}

func (server *HttpServer) Start() {
	r := chi.NewRouter()

	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		ExposedHeaders:   []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           int(12 * time.Hour),
	}))

	r.Route("/", func(r chi.Router) {
		r.Post("/api/register", server.userController.Signup)
		r.Post("/api/authorize", server.userController.Login)
		r.Post("/api/initForgotPassword", server.userController.InitForgotPassword)
		r.HandleFunc("/oauth", server.userController.HandleGoogleLogin)
		r.HandleFunc("/callback", server.userController.HandleGoogleCallback)

		r.HandleFunc("/forgotPassword", server.templateController.ForgotPassword)
		r.HandleFunc("/signup", server.templateController.SignUpUI)
		r.HandleFunc("/redirect", server.templateController.Redirect)
		r.HandleFunc("/", server.templateController.Login)
		r.HandleFunc("/reset-password", server.templateController.UpdatePassword)

	})

	r.Route("/updatePassword", func(validate chi.Router) {
		validate.Use(server.middleWare.ValidateForgetPasswordToken)
		validate.Post("/updatePasswordApi", server.userController.UpdatePassword)
	})

	r.Route("/api/auth", func(private chi.Router) {
		private.Use(server.middleWare.ValidateToken)
		private.Get("/getProfile", server.userController.GetProfile)
		private.Post("/createProfile", server.userController.CreateProfile)
		private.Post("/updateProfile", server.userController.UpdateProfile)

	})

	r.Route("/user", func(private chi.Router) {
		//////////////////////////Templates
		private.HandleFunc("/setProfile", server.templateController.CreateProfile)
		private.HandleFunc("/showProfile", server.templateController.GetProfile)
	})
	err := http.ListenAndServe(server.addr, r)
	if err != nil {
		panic(err)
	}

}
