package rest

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetProfile(t *testing.T) {
	t.Run("Should return some profile", func(t *testing.T) {
		testService := NewTestService(t)
		mockProfile := GetMockProfile()
		mockStore := testService.GetMockStore()
		mockService := testService.GetMockUserService()
		mockService.EXPECT().GetProfile("ID").Return(mockProfile, nil).AnyTimes()
		mockStore.EXPECT().GetProfile("ID").Return(mockProfile, nil).AnyTimes()

		assert.Equal(t, mockProfile, mockProfile, "")
	})
}
