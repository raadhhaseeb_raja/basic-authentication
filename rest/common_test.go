package rest

import (
	"github.com/GoChallenge/GoChallengeAuth/mocks/service"
	models2 "github.com/GoChallenge/GoChallengeAuth/mocks/store"
	"github.com/GoChallenge/GoChallengeAuth/models"
	"github.com/golang/mock/gomock"
	"testing"
)

type TestService interface {
	GetMockUserService() *service.MockUserService
	GetMockStore() *models2.MockStore
}

type testService struct {
	ctrl *gomock.Controller
}

func (t *testService) GetMockUserService() *service.MockUserService {
	return  service.NewMockUserService(t.ctrl)
}

func (t *testService) GetMockStore() *models2.MockStore {
	return models2.NewMockStore(t.ctrl)
}

func NewTestService(t *testing.T) TestService {
	mockCtrl := gomock.NewController(t)
	return &testService{
		ctrl: mockCtrl,
	}
}

func GetMockProfile() *models.Profile {
	return &models.Profile{
		Age:         "15",
		Name:        "Some Name",
		Education:   "Some edu",
		Nationality: "PK",
		Phone:       "Some phone",
		UID:         "SomerandomUID",
	}
}
