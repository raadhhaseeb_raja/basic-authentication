package rest

import (
	"context"
	"github.com/GoChallenge/GoChallengeAuth/models"
	"github.com/GoChallenge/GoChallengeAuth/service"
	"log"
	"net/http"
)

type MiddleWare interface {
	ValidateToken(next http.Handler) http.Handler
	ValidateForgetPasswordToken(next http.Handler) http.Handler
}

type middleWare struct {
	userService service.UserService
	jwtService  service.JWTService
}

//NewAuthController return new instance of authcontroller
func NewMiddleWare(userService service.UserService, jwtService service.JWTService) MiddleWare {

	return &middleWare{
		userService: userService,
		jwtService:  jwtService,
	}
}

func (m *middleWare) ValidateForgetPasswordToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		token := r.Header.Get("token")
		if len(token) == 0 {

			WriteErrorJson(w, models.StandardResponse{
				Result:  false,
				Code:    403,
				Message: "Invalid Token",
				Data:    nil,
			}, 403)
			return
		}
		claims, err := m.jwtService.ValidateForgetPasswordToken(token)
		if err != nil {
			WriteErrorJson(w, models.StandardResponse{
				Result:  false,
				Code:    403,
				Message: "Invalid Token",
				Data:    nil,
			}, 403)
			return
		}
		currentUser := &models.HasForgottenPassword{
			Secret: claims.Secret,
			Email:  claims.Email,
		}
		ctx := context.WithValue(r.Context(), models.KeyCurrentContext, currentUser)
		next.ServeHTTP(w, r.WithContext(ctx))

	})
}

func (m *middleWare) ValidateToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		token := r.Header.Get("token")
		if len(token) == 0 {
			WriteErrorJson(w, models.StandardResponse{
				Result:  false,
				Code:    403,
				Message: "Invalid Token",
				Data:    nil,
			}, 403)
			return
		}

		claims, err := m.jwtService.ValidateToken(token)
		if err != nil {
			WriteErrorJson(w, models.StandardResponse{
				Result:  false,
				Code:    403,
				Message: "Invalid Token",
				Data:    nil,
			}, 403)
			return
		}
		uid := claims.UID
		log.Println(uid)
		ctx := context.WithValue(r.Context(), models.KeyCurrentUser, uid)
		next.ServeHTTP(w, r.WithContext(ctx))

	})
}
