package repository

import (
	"github.com/GoChallenge/GoChallengeAuth/models"
	"net/http"
)

type Store interface {
	BeginTx() (Store, error)
	Rollback() error
	CommitTx() error

	ForgotPassword(ForgotPass *models.ForgotPasswordDetails) *models.StandardResponse
	UpdatePassword(user *models.User) *models.StandardResponse
	Login(w http.ResponseWriter, r *http.Request)
	Signup(user models.User) *models.StandardResponse
	UpdateProfile(profile *models.Profile) (*models.Profile, *models.StandardResponse)
	GetProfile(UID string) (*models.Profile, *models.StandardResponse)
	GetUserByEmail(email string) (*models.User, *models.StandardResponse)
	CreateProfile(profile models.Profile) (*models.Profile, *models.StandardResponse)
	IsSecretCorrect(email, status, secret string) (*models.ForgotPasswordDetails, *models.StandardResponse)
	NullAllPasswordChangeRequests(email, status string) *models.StandardResponse
	LoginWithGoogle(login *models.GoogleAuthSignIn) *models.StandardResponse
}
