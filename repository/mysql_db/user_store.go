package mysql_db

import (
	"github.com/GoChallenge/GoChallengeAuth/models"
	"github.com/jinzhu/gorm"
	"log"
	"net/http"
	"strings"
)

func (s *Store) ForgotPassword(ForgotPass *models.ForgotPasswordDetails) *models.StandardResponse {
	err := s.db.Create(&ForgotPass).Error
	if err != nil {
		return models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	return nil
}

func (s *Store) UpdatePassword(user *models.User) *models.StandardResponse {
	result := s.db.Table("user").Where("email = ?", user.Email).Update("password", user.Password)
	if result.Error != nil {
		return models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	return nil
}

func (s *Store) Login(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (s *Store) Signup(user models.User) *models.StandardResponse {
	err := s.db.Create(&user).Error
	if err != nil {
		if strings.Contains(err.Error(), "Duplicate") {
			return models.NewStandardResponse(false, 1062, "User Already Exists", nil)
		}
		return models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	return nil
}

func (s *Store) UpdateProfile(profile *models.Profile) (*models.Profile, *models.StandardResponse) {
	result := s.db.Table("profile").Where("uid = ?", profile.UID).Update("age", profile.Age).Update("name", profile.Name).Update("education", profile.Education).Update("nationality", profile.Nationality).Update("phone", profile.Phone)
	if result.Error != nil {
		return nil, models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	return profile, nil
}

func (s *Store) GetProfile(UID string) (*models.Profile, *models.StandardResponse) {
	log.Println(UID)
	var profile models.Profile
	result := s.db.Table("profile").First(&profile, "uid = ?", UID)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound || result.RecordNotFound() || strings.Contains(result.Error.Error(), "expected 0") {
			return nil, models.NewStandardResponse(false, 403, "No Profile Exists for Current User", nil)
		}
		return nil, models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	return &profile, nil
}

func (s *Store) GetUserByEmail(email string) (*models.User, *models.StandardResponse) {
	var user models.User
	err := s.db.Where("email = ?", email).First(&user).Error
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return nil, models.NewStandardResponse(false, 403, "User does not exist", nil)
		}
		return nil, models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	return &user, nil
}

func (s *Store) CreateProfile(profile models.Profile) (*models.Profile, *models.StandardResponse) {
	err := s.db.Create(&profile).Error
	if err != nil {
		if strings.Contains(err.Error(), "Duplicate") {
			if strings.Contains(err.Error(), "for key 'profile.phone'") {
				return nil, models.NewStandardResponse(false, 1062, "Phone Number already exists", nil)
			}
			return nil, models.NewStandardResponse(false, 1062, "Profile Already Exists", nil)
		}
		return nil, models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	return &profile, nil
}

func (s *Store) IsSecretCorrect(email, status, secret string) (*models.ForgotPasswordDetails, *models.StandardResponse) {
	var details models.ForgotPasswordDetails
	result := s.db.Table("forgot_password_details").Where("email = ? AND secret = ? AND status = ?", email, secret, status).Find(&details)
	if result.Error != nil {
		//InCorrect Secret Error
		return nil, models.NewStandardResponse(false, http.StatusBadRequest, "Change password request not generated or Incorrect Details", nil)
	}
	return &details, nil
}

func (s *Store) NullAllPasswordChangeRequests(email, status string) *models.StandardResponse {
	result := s.db.Table("forgot_password_details").Where("email = ?", email).Update("status", status)
	if result.Error != nil {
		return models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	return nil
}

func (s *Store) LoginWithGoogle(login *models.GoogleAuthSignIn) *models.StandardResponse {
	err := s.db.Create(&login).Error
	if err != nil {
		if strings.Contains(err.Error(), "Duplicate") {
			return models.NewStandardResponse(false, 1062, "User Already Exists", nil)
		}
		return models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	return nil
}
