package mysql_db

import (
	"fmt"
	"github.com/GoChallenge/GoChallengeAuth/conf"
	"github.com/GoChallenge/GoChallengeAuth/models"
	"github.com/GoChallenge/GoChallengeAuth/repository"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"log"
	"strconv"
	"sync"
	"time"
)

var gdb *gorm.DB
var storeOnce sync.Once
var store *Store

type Store struct {
	db *gorm.DB
}

//SharedStore return global or single instance of mysql connection (bounded in sync once)
func SharedStore() Store {
	storeOnce.Do(func() {
		err := initDb()
		if err != nil {
			panic(err)
		}
		store = NewStore(gdb)
	})
	return *store
}

func NewStore(db *gorm.DB) *Store {
	return &Store{
		db: db,
	}
}

func initDb() error {
	cfg := conf.GetConfig()
	config := fmt.Sprintf(
		"%s:%s@%s(%s:%s)/%s", cfg.MySQL.User, cfg.MySQL.Password, "tcp", cfg.MySQL.Addr, strconv.Itoa(int(cfg.MySQL.Port)), cfg.MySQL.Database,
	)
	retries := cfg.MySQL.Retries
	log.Println(config)
	var err error

	gdb, err = gorm.Open("mysql", config)
	for err != nil {
		log.Println(err, fmt.Sprintf("Failed to connect to database (%d)", retries))

		if retries > 1 {
			retries--
			time.Sleep(5 * time.Second)
			gdb, err = gorm.Open("mysql", config)
			continue
		}
		panic(err)
	}
	gdb.SingularTable(true)
	gdb.DB().SetMaxIdleConns(10)
	gdb.DB().SetMaxOpenConns(50)
	if cfg.MySQL.EnableAutoMigrate {
		var tables = []interface{}{
			&models.User{},
			&models.Profile{},
			&models.ForgotPasswordDetails{},
			&models.GoogleAuthSignIn{},
		}

		for _, table := range tables {
			gdb.AutoMigrate(table)
		}
	}

	return nil
}

func (s *Store) BeginTx() (repository.Store, error) {
	db := s.db.Begin()
	if db.Error != nil {
		return nil, db.Error
	}
	return NewStore(db), nil
}

func (s *Store) Rollback() error {
	return s.db.Rollback().Error
}

func (s *Store) CommitTx() error {
	return s.db.Commit().Error
}
