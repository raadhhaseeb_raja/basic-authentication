package goTemplates

import (
	"github.com/GoChallenge/GoChallengeAuth/models"
	"github.com/GoChallenge/GoChallengeAuth/service"
	"html/template"
	"log"
	"net/http"
	//rest"github.com/GoChallenge/GoChallengeAuth/rest"
)

type TemplateController interface {
	SignUpUI(w http.ResponseWriter, r *http.Request)
	CreateProfile(w http.ResponseWriter, r *http.Request)
	GetProfile(w http.ResponseWriter, r *http.Request)
	UpdateProfile(w http.ResponseWriter, r *http.Request)
	ForgotPassword(w http.ResponseWriter, r *http.Request)
	UpdatePassword(w http.ResponseWriter, r *http.Request)
	Login(w http.ResponseWriter, r *http.Request)
	Redirect(w http.ResponseWriter, r *http.Request)
}

type templateController struct {
	userservice service.UserService
	jwtService  service.JWTService
}

func NewTemplateController(jwtService service.JWTService, userService service.UserService) TemplateController {
	return &templateController{
		userservice: userService,
		jwtService:  jwtService,
	}
}

func (t templateController) SignUpUI(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		signupTemp, err := template.ParseFiles("./web/goTemplates/html/signup.html")
		if err != nil {
			log.Println(err)
		}
		err = signupTemp.Execute(w, nil)
		if err != nil {
			return
		}
	}
}

func (t templateController) CreateProfile(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		signupTemp, _ := template.ParseFiles("./web/goTemplates/html/createProfile.html")
		session := models.Session{Token: r.URL.Query().Get("token")}
		err := signupTemp.Execute(w, session)
		if err != nil {
			return
		}
	}
}

func (t templateController) GetProfile(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		token := r.Header.Get("token")
		signupTemp, _ := template.ParseFiles("./web/goTemplates/html/getProfile.html")
		log.Println(token)
		err := signupTemp.Execute(w, nil)
		if err != nil {
			return
		}
		user, err := t.jwtService.ValidateToken(r.Header.Get("token"))
		if err != nil {
			return
		}
		getProfile, sr := t.userservice.GetProfile(user.UID)
		if sr != nil {
			return
		}
		log.Println(getProfile)

	}
}

func (t templateController) UpdateProfile(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		signupTemp, _ := template.ParseFiles("./web/goTemplates/html/createProfile.html")
		r.AddCookie(&http.Cookie{
			Value: r.URL.Query().Get("token"),
			Name:  "token",
		})
		session := models.Session{Token: r.URL.Query().Get("token")}
		err := signupTemp.Execute(w, session)
		if err != nil {
			return
		}
	}
}

func (t templateController) ForgotPassword(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		loginTemp, err := template.ParseFiles("./web/goTemplates/html/forgotPassword.html")
		if err != nil {
			log.Println(err)
		}
		session := models.Session{Token: r.URL.Query().Get("token")}
		err = loginTemp.Execute(w, session)
		if err != nil {
			return
		}
	}
}

func (t templateController) UpdatePassword(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {

		updateTemp, err := template.ParseFiles("./web/goTemplates/html/updatePassword.html")
		if err != nil {
			log.Println(err)
		}
		session := models.Session{Token: r.URL.Query().Get("token")}
		err = updateTemp.Execute(w, session)
		if err != nil {
			return
		}
	}
}

func (t templateController) Login(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		signupTemp, err := template.ParseFiles("./web/goTemplates/html/login.html")
		if err != nil {
			log.Println(err)
		}
		err = signupTemp.Execute(w, nil)
		if err != nil {
			return
		}
	}
}

func (t templateController) Redirect(w http.ResponseWriter, r *http.Request) {
	r.AddCookie(&http.Cookie{
		Value: r.URL.Query().Get("token"),
		Name:  "token",
	})
	redirectTemp, _ := template.ParseFiles("./web/goTemplates/html/redirect.html")
	session := models.Session{Token: r.URL.Query().Get("token")}
	err := redirectTemp.Execute(w, session)
	if err != nil {
		return
	}
}
