package main

import (
	"fmt"
	"github.com/GoChallenge/GoChallengeAuth/logger"
	"github.com/GoChallenge/GoChallengeAuth/rest"
	"github.com/GoChallenge/GoChallengeAuth/service"
)

func main() {
	logger.Init()

	logger.Instance().Info("#==================================#")
	logger.Instance().Info("#===========Starting Server =======#")
	logger.Instance().Info("#==================================#")

	serviceContainer := service.NewServiceContainer()

	/*
	* Initiate Rest Server
	 */
	rest.StartServer(serviceContainer)

	fmt.Println("========== Rest Server Started ============")
	logger.Instance().Println("========== Server Stated ============")
	select {}

}
