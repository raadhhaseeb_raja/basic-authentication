LABEL=$(shell git log -1 --format=%h)


docker-build:
	@docker build -t goChallenge/go-auth:$(LABEL) .

mock-user-service:
	@mockgen -destination=./mocks/service/mock_user_service.go -package=service github.com/GoChallenge/GoChallengeAuth/service  UserService

mock-store-service:
	@mockgen -destination=./mocks/store/mock_store_service.go -package=models github.com/GoChallenge/GoChallengeAuth/repository  Store

gen-mock: mock-store-service mock-user-service

