// Code generated by MockGen. DO NOT EDIT.
// Source: github.com/GoChallenge/GoChallengeAuth/repository (interfaces: Store)

// Package models is a generated GoMock package.
package models

import (
	models "github.com/GoChallenge/GoChallengeAuth/models"
	repository "github.com/GoChallenge/GoChallengeAuth/repository"
	gomock "github.com/golang/mock/gomock"
	http "net/http"
	reflect "reflect"
)

// MockStore is a mock of Store interface
type MockStore struct {
	ctrl     *gomock.Controller
	recorder *MockStoreMockRecorder
}

// MockStoreMockRecorder is the mock recorder for MockStore
type MockStoreMockRecorder struct {
	mock *MockStore
}

// NewMockStore creates a new mock instance
func NewMockStore(ctrl *gomock.Controller) *MockStore {
	mock := &MockStore{ctrl: ctrl}
	mock.recorder = &MockStoreMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockStore) EXPECT() *MockStoreMockRecorder {
	return m.recorder
}

// BeginTx mocks base method
func (m *MockStore) BeginTx() (repository.Store, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "BeginTx")
	ret0, _ := ret[0].(repository.Store)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// BeginTx indicates an expected call of BeginTx
func (mr *MockStoreMockRecorder) BeginTx() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "BeginTx", reflect.TypeOf((*MockStore)(nil).BeginTx))
}

// CommitTx mocks base method
func (m *MockStore) CommitTx() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CommitTx")
	ret0, _ := ret[0].(error)
	return ret0
}

// CommitTx indicates an expected call of CommitTx
func (mr *MockStoreMockRecorder) CommitTx() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CommitTx", reflect.TypeOf((*MockStore)(nil).CommitTx))
}

// CreateProfile mocks base method
func (m *MockStore) CreateProfile(arg0 models.Profile) (*models.Profile, *models.StandardResponse) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateProfile", arg0)
	ret0, _ := ret[0].(*models.Profile)
	ret1, _ := ret[1].(*models.StandardResponse)
	return ret0, ret1
}

// CreateProfile indicates an expected call of CreateProfile
func (mr *MockStoreMockRecorder) CreateProfile(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateProfile", reflect.TypeOf((*MockStore)(nil).CreateProfile), arg0)
}

// ForgotPassword mocks base method
func (m *MockStore) ForgotPassword(arg0 *models.ForgotPasswordDetails) *models.StandardResponse {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "InitForgotPassword", arg0)
	ret0, _ := ret[0].(*models.StandardResponse)
	return ret0
}

// ForgotPassword indicates an expected call of ForgotPassword
func (mr *MockStoreMockRecorder) ForgotPassword(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InitForgotPassword", reflect.TypeOf((*MockStore)(nil).ForgotPassword), arg0)
}

// GetProfile mocks base method
func (m *MockStore) GetProfile(arg0 string) (*models.Profile, *models.StandardResponse) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetProfile", arg0)
	ret0, _ := ret[0].(*models.Profile)
	ret1, _ := ret[1].(*models.StandardResponse)
	return ret0, ret1
}

// GetProfile indicates an expected call of GetProfile
func (mr *MockStoreMockRecorder) GetProfile(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetProfile", reflect.TypeOf((*MockStore)(nil).GetProfile), arg0)
}

// GetUserByEmail mocks base method
func (m *MockStore) GetUserByEmail(arg0 string) (*models.User, *models.StandardResponse) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetUserByEmail", arg0)
	ret0, _ := ret[0].(*models.User)
	ret1, _ := ret[1].(*models.StandardResponse)
	return ret0, ret1
}

// GetUserByEmail indicates an expected call of GetUserByEmail
func (mr *MockStoreMockRecorder) GetUserByEmail(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetUserByEmail", reflect.TypeOf((*MockStore)(nil).GetUserByEmail), arg0)
}

// IsSecretCorrect mocks base method
func (m *MockStore) IsSecretCorrect(arg0, arg1, arg2 string) (*models.ForgotPasswordDetails, *models.StandardResponse) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IsSecretCorrect", arg0, arg1, arg2)
	ret0, _ := ret[0].(*models.ForgotPasswordDetails)
	ret1, _ := ret[1].(*models.StandardResponse)
	return ret0, ret1
}

// IsSecretCorrect indicates an expected call of IsSecretCorrect
func (mr *MockStoreMockRecorder) IsSecretCorrect(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IsSecretCorrect", reflect.TypeOf((*MockStore)(nil).IsSecretCorrect), arg0, arg1, arg2)
}

// Login mocks base method
func (m *MockStore) Login(arg0 http.ResponseWriter, arg1 *http.Request) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Login", arg0, arg1)
}

// Login indicates an expected call of Login
func (mr *MockStoreMockRecorder) Login(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Login", reflect.TypeOf((*MockStore)(nil).Login), arg0, arg1)
}

// NullAllPasswordChangeRequests mocks base method
func (m *MockStore) NullAllPasswordChangeRequests(arg0, arg1 string) *models.StandardResponse {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "NullAllPasswordChangeRequests", arg0, arg1)
	ret0, _ := ret[0].(*models.StandardResponse)
	return ret0
}

// NullAllPasswordChangeRequests indicates an expected call of NullAllPasswordChangeRequests
func (mr *MockStoreMockRecorder) NullAllPasswordChangeRequests(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "NullAllPasswordChangeRequests", reflect.TypeOf((*MockStore)(nil).NullAllPasswordChangeRequests), arg0, arg1)
}

// Rollback mocks base method
func (m *MockStore) Rollback() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Rollback")
	ret0, _ := ret[0].(error)
	return ret0
}

// Rollback indicates an expected call of Rollback
func (mr *MockStoreMockRecorder) Rollback() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Rollback", reflect.TypeOf((*MockStore)(nil).Rollback))
}

// Signup mocks base method
func (m *MockStore) Signup(arg0 models.User) *models.StandardResponse {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Signup", arg0)
	ret0, _ := ret[0].(*models.StandardResponse)
	return ret0
}

// Signup indicates an expected call of Signup
func (mr *MockStoreMockRecorder) Signup(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Signup", reflect.TypeOf((*MockStore)(nil).Signup), arg0)
}

// UpdatePassword mocks base method
func (m *MockStore) UpdatePassword(arg0 *models.User) *models.StandardResponse {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdatePassword", arg0)
	ret0, _ := ret[0].(*models.StandardResponse)
	return ret0
}

// UpdatePassword indicates an expected call of UpdatePassword
func (mr *MockStoreMockRecorder) UpdatePassword(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdatePassword", reflect.TypeOf((*MockStore)(nil).UpdatePassword), arg0)
}

// UpdateProfile mocks base method
func (m *MockStore) UpdateProfile(arg0 *models.Profile) (*models.Profile, *models.StandardResponse) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "UpdateProfile", arg0)
	ret0, _ := ret[0].(*models.Profile)
	ret1, _ := ret[1].(*models.StandardResponse)
	return ret0, ret1
}

// UpdateProfile indicates an expected call of UpdateProfile
func (mr *MockStoreMockRecorder) UpdateProfile(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "UpdateProfile", reflect.TypeOf((*MockStore)(nil).UpdateProfile), arg0)
}
