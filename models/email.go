package models

type Email struct {
	Address string `json:"address"`
	Subject string `json:"subject"`
	Body    string `json:"body"`
}

