package models

import (
	"golang.org/x/crypto/bcrypt"
	"log"
	"math/rand"
	"regexp"
	"strings"
	"time"
)

const EMAILREQACTIVE = "ACTIVE"
const EMAILREQINACTIVE = "INACTIVE"

type User struct {
	UID      string `gorm:"unique;not null"`
	Email    string `gorm:"unique;not null"`
	Password string
}

type AuthenticatedUser struct {
	UID   string `json:"uid"`
	Email string `json:"email"`
	Token string `json:"token"`
}

type CurrentUser struct {
	UID   string `json:"uid"`
	Email string `json:"email"`
}

type HasForgottenPassword struct {
	Email  string `json:"email"`
	Secret string `json:"secret"`
}

func (u User) Validate() bool {
	if !isEmailValid(u.Email) || !isPasswordValid(u.Password) {
		return false
	}
	return true
}

func isPasswordValid(e string) bool {
	if len(e) < 6 {
		return false
	}
	return true
}

func isEmailValid(e string) bool {
	emailRegex := regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`)
	emailRegexp := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if emailRegex.MatchString(e) == false || emailRegexp.MatchString(e) == false {
		return false
	}
	return true
}

func (u *User) EncryptPassword() []byte {
	password, _ := bcrypt.GenerateFromPassword([]byte(u.Password), 14)
	return password
}

func (u *User) DecryptPassword(password string) error {
	log.Println(password)
	return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
}

func (profile *Profile) Validate() bool {
	if len(strings.TrimSpace(profile.Phone)) == 0 && len(strings.TrimSpace(profile.UID)) == 0 {
		return false
	}
	return true
}

func RandStringRunes() string {
	var letterRunes = []rune("1234567890")
	b := make([]rune, 6)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

type Session struct {
	Token        string `gorm:"unique;not null"`
	UserID       string `gorm:"unique;not null"`
	LoginTime    time.Time
	LastSeenTime time.Time
	State        bool
}
