package models

import (
	"github.com/GoChallenge/GoChallengeAuth/conf"
	"github.com/dgrijalva/jwt-go"
)

////////////////////////////////////////////Rest Consts
const KeyCurrentUser = "__current_user"
const KeyCurrentContext = "__current_context"

////////////////////////////////////////////JWT CLAIMS OBJECT

type JWTToken struct {
	Token string `json:"token"`
}

type DecodeJWTClaims struct {
	ExpiredAt       float64 `json:"expiredAt"`
	UID             string  `json:"uid"`
	ActiveAccountID string  `json:"activeAccountID"`
	AccountType     string  `json:"accountType"`
}

// JwtWrapper wraps the signing key and the issuer
type JwtWrapper struct {
	SecretKey       string
	Issuer          string
	ExpirationHours int64
}

// JwtClaim adds email as a claim to the token
type JwtClaim struct {
	Email string
	UID   string
	jwt.StandardClaims
}

// ForgetPasswordJwtClaim adds email as a claim to the token
type ForgetPasswordJwtClaim struct {
	Secret string
	Email  string
	jwt.StandardClaims
}

func GetJWTWrapper() *JwtWrapper {
	return &JwtWrapper{
		SecretKey:       conf.GetConfig().JwtSecret,
		Issuer:          "goChallenge",
		ExpirationHours: 1,
	}
}

/////////////////////////////////////////////Standard Error Response
type StandardResponse struct {
	Result  bool        `json:"result"`
	Code    uint        `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func NewStandardResponse(result bool, code uint, msg string, data interface{}) *StandardResponse {

	// Nil data would be set to empty
	if data == nil {
		data = ""
	}
	return &StandardResponse{
		Result:  result,
		Code:    code,
		Message: msg,
		Data:    data,
	}
}

/////////////////////////////////////User Signup Request

type UserRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Profile struct {
	Age         string `json:"age"`
	Name        string `json:"name"`
	Education   string `json:"education"`
	Nationality string `json:"nationality"`
	Phone       string `json:"phone" gorm:"unique;not null"`
	UID         string `json:"uid" gorm:"unique;not null"`
}

type UpdatePasswordRequest struct {
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirmPassword"`
}

type ForgetPasscode struct {
	Email string `json:"email"`
}

type ForgotPasswordDetails struct {
	UserId string `json:"userId"`
	Email  string `json:"email"`
	Secret string `json:"secret"`
	Status string `json:"status"`
	Time   string `json:"time"`
}

type GoogleAuthSignIn struct {
	ID            string `json:"id" gorm:"unique;not null"`
	Email         string `json:"email" gorm:"unique;not null"`
	VerifiedEmail bool   `json:"verified_email"`
}
