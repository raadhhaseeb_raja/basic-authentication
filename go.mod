module github.com/GoChallenge/GoChallengeAuth

go 1.17

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.2.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/mock v1.4.4
	github.com/jinzhu/gorm v1.9.16
	github.com/rogierlommers/logrus-redis-hook v0.0.0-20210902125448-a9c74cf92631
	github.com/rs/xid v1.3.0
	github.com/sendgrid/sendgrid-go v3.10.4+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.4.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	golang.org/x/oauth2 v0.0.0-20211104180415-d3ed0bb246c8
)

require (
	cloud.google.com/go v0.65.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/garyburd/redigo v1.6.3 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sendgrid/rest v2.6.6+incompatible // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
