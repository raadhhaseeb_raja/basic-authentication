package service

import (
	"github.com/GoChallenge/GoChallengeAuth/models"
	"github.com/GoChallenge/GoChallengeAuth/repository"
	"net/http"
	"strings"
	"time"
)

type UserService interface {
	ForgotPassword(req models.ForgetPasscode, service JWTService) (string, *models.StandardResponse)
	UpdatePassword(user *models.HasForgottenPassword, details *models.UpdatePasswordRequest) (*models.User, *models.StandardResponse)
	Login(user *models.User) (*models.User, *models.StandardResponse)
	Signup(user models.User) *models.StandardResponse
	UpdateProfile(profile *models.Profile) (*models.Profile, *models.StandardResponse)
	GetProfile(user string) (*models.Profile, *models.StandardResponse)
	CreateProfile(profile models.Profile) (*models.Profile, *models.StandardResponse)
	SendEmail(email, token string) error
	LoginWithGoogle(login *models.GoogleAuthSignIn) *models.StandardResponse
}

type userService struct {
	store  repository.Store
	mailer Mailer
}

func NewUserService(mailer Mailer, store repository.Store) UserService {
	return &userService{
		store:  store,
		mailer: mailer,
	}
}

func (u userService) ForgotPassword(req models.ForgetPasscode, service JWTService) (string, *models.StandardResponse) {
	getUser, resp := u.store.GetUserByEmail(req.Email)
	if resp != nil {
		return "", resp
	}
	SecretCode := models.RandStringRunes()
	token, _ := service.GenerateForgotPasswordToken(req.Email, SecretCode)
	//JWTToken := GetToken(token)
	err := u.SendEmail(getUser.Email, token)
	if err != nil {
		return "", models.NewStandardResponse(false, http.StatusInternalServerError, "Email not sent", nil)
	}
	forgotPasswordDetails := &models.ForgotPasswordDetails{
		UserId: getUser.UID,
		Email:  getUser.Email,
		Secret: SecretCode,
		Status: models.EMAILREQACTIVE,
		Time:   time.Now().Format(time.RFC3339),
	}

	return token, u.store.ForgotPassword(forgotPasswordDetails)
}

func (u userService) SendEmail(emailAddr, tokens string) error {
	email := &models.Email{
		Address: emailAddr,
		Subject: "Forgot Password",
		Body:    tokens,
	}

	resp := u.mailer.SendMail(email)
	if resp != nil {
		return resp
	}
	return nil
}

func (u userService) UpdatePassword(hasForgotten *models.HasForgottenPassword, updatePasswordRequest *models.UpdatePasswordRequest) (*models.User, *models.StandardResponse) {
	userDetails, _ := u.store.GetUserByEmail(hasForgotten.Email)
	if userDetails == nil {
		return nil, models.NewStandardResponse(false, http.StatusBadRequest, "User does not exist", nil)
	}
	isSecretCorrect, sr := u.store.IsSecretCorrect(userDetails.Email, models.EMAILREQACTIVE, hasForgotten.Secret)
	if sr != nil || isSecretCorrect == nil {
		return nil, sr
	}
	if isSecretCorrect.Secret != hasForgotten.Secret {
		return nil, models.NewStandardResponse(false, http.StatusBadRequest, "Request Does not exist", nil)
	}
	userDetails.Password = updatePasswordRequest.Password
	userDetails.Password = string(userDetails.EncryptPassword())
	sr = u.store.UpdatePassword(userDetails)
	if sr != nil {
		return nil, models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	sr = u.store.NullAllPasswordChangeRequests(userDetails.Email, models.EMAILREQINACTIVE)
	if sr != nil {
		return nil, models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	return userDetails, nil
}

func (u userService) Login(user *models.User) (*models.User, *models.StandardResponse) {
	isValid := user.Validate()
	if !isValid {
		return nil, models.NewStandardResponse(false, http.StatusBadRequest, "Invalid Input", nil)
	}
	userDetails, sr := u.store.GetUserByEmail(user.Email)
	if userDetails == nil {
		return nil, models.NewStandardResponse(false, http.StatusBadRequest, "User does not exist", nil)
	}
	sr = u.store.NullAllPasswordChangeRequests(userDetails.Email, models.EMAILREQINACTIVE)
	if sr != nil {
		return nil, models.NewStandardResponse(false, 500, "Database Error", nil)
	}
	isAutheticUser := userDetails.DecryptPassword(user.Password)
	if isAutheticUser != nil {
		return nil, models.NewStandardResponse(false, http.StatusBadRequest, "Incorrect Email or Password", nil)
	}
	return userDetails, nil
}

func (u userService) Signup(user models.User) *models.StandardResponse {
	// Checks if password and Email entered by User are Valid
	isValid := user.Validate()
	if !isValid {
		return models.NewStandardResponse(false, http.StatusBadRequest, "Invalid Input", nil)
	}
	user.Password = string(user.EncryptPassword())
	return u.store.Signup(user)
}

func (u userService) UpdateProfile(profile *models.Profile) (*models.Profile, *models.StandardResponse) {
	isValidProfile := profile.Validate()
	if !isValidProfile {
		return nil, models.NewStandardResponse(false, http.StatusBadRequest, "Incorrect Email or Password", nil)
	}
	return u.store.UpdateProfile(profile)
}

func (u userService) GetProfile(user string) (*models.Profile, *models.StandardResponse) {
	profile, sr := u.store.GetProfile(user)

	if sr != nil {
		return nil, sr
	}
	return profile, nil
}

func (u userService) CreateProfile(profile models.Profile) (*models.Profile, *models.StandardResponse) {
	isValidProfile := profile.Validate()
	if !isValidProfile {
		return nil, models.NewStandardResponse(false, http.StatusBadRequest, "Incorrect Email or Password", nil)
	}
	return u.store.CreateProfile(profile)
}

func (u userService) LoginWithGoogle(login *models.GoogleAuthSignIn) *models.StandardResponse {
	user, sr := u.store.GetUserByEmail(login.Email)
	if sr != nil && !strings.Contains(sr.Message, "User does not exist") {
		return sr
	}
	if user != nil {
		return &models.StandardResponse{
			Result:  false,
			Code:    400,
			Message: "already registered via email signup",
			Data:    nil,
		}
	}
	return u.store.LoginWithGoogle(login)
}
