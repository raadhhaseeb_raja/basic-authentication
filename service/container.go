package service

import (
	"github.com/GoChallenge/GoChallengeAuth/repository"
	"github.com/GoChallenge/GoChallengeAuth/repository/mysql_db"
	"log"
)

type Container struct {
	GbeConfigService GbeConfigService
	JWTService       JWTService
	Logger           LoggerService
	Mailer           Mailer
	Store            repository.Store
	UserService      UserService
}

// creates a container by initiating all required services
func NewServiceContainer() *Container {
	//Get Configs
	//gbeConfig := conf.GetConfig()
	gbeConfigService := NewGbeConfigService()

	//Get Store
	store := mysql_db.SharedStore()

	//Get Logger Service
	loggerService := NewLoggerService()

	// Get Mailer Service
	sendgridMailer := gbeConfigService.GetConfig().SendGrid
	log.Println(sendgridMailer)
	mailerService := NewMailer(sendgridMailer.ApiKey, sendgridMailer.SenderName, sendgridMailer.SenderEmail)

	//Get Auth Service
	//Get User Service
	userService := NewUserService(mailerService, &store)

	//Get JWT Services
	jwtService := NewJWTService(gbeConfigService, userService)

	return &Container{
		GbeConfigService: gbeConfigService,
		JWTService:       jwtService,
		Logger:           loggerService,
		Mailer:           mailerService,
		Store:            &store,
		UserService:      userService,
	}
}
