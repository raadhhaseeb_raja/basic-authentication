package service

import (
	"fmt"
	"github.com/GoChallenge/GoChallengeAuth/models"
	"github.com/dgrijalva/jwt-go"
	"time"
)

type JWTService interface {
	GenerateToken(email, uid string) (signedToken string, err error)
	GenerateForgotPasswordToken(email, secret string) (signedToken string, err error)
	ValidateToken(usertoken string) (*models.JwtClaim, error)
	ValidateForgetPasswordToken(usertoken string) (*models.ForgetPasswordJwtClaim, error)
}
type jwtService struct {
	gbeConfig   GbeConfigService
	userService UserService
}

func NewJWTService(gbeConfig GbeConfigService, userService UserService) JWTService {
	return &jwtService{
		gbeConfig:   gbeConfig,
		userService: userService,
	}
}

// GenerateToken generates a jwt token
func (j *jwtService) GenerateToken(email, uid string) (signedToken string, err error) {

	jwtWrapper := models.GetJWTWrapper()
	claims := &models.JwtClaim{
		Email: email,
		UID:   uid,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Local().Add(time.Hour * time.Duration(jwtWrapper.ExpirationHours)).Unix(),
			Issuer:    jwtWrapper.Issuer,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signedToken, err = token.SignedString([]byte(jwtWrapper.SecretKey))
	if err != nil {
		return
	}

	return
}

func (j *jwtService) GenerateForgotPasswordToken(email, secret string) (signedToken string, err error) {
	jwtWrapper := models.GetJWTWrapper()
	claims := &models.ForgetPasswordJwtClaim{
		Email:  email,
		Secret: secret,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Local().Add(time.Minute * time.Duration(15)).Unix(),
			Issuer:    jwtWrapper.Issuer,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signedToken, err = token.SignedString([]byte(jwtWrapper.SecretKey))
	if err != nil {
		return
	}

	return
}

//ValidateToken validates the jwt token
func (j *jwtService) ValidateToken(usertoken string) (*models.JwtClaim, error) {
	jwtWrapper := models.GetJWTWrapper()
	token, err := jwt.ParseWithClaims(
		usertoken,
		&models.JwtClaim{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(jwtWrapper.SecretKey), nil
		},
	)

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*models.JwtClaim)
	if !ok {
		return nil, fmt.Errorf("Couldn't parse claims")
	}

	if claims.ExpiresAt < time.Now().Local().Unix() {
		return nil, fmt.Errorf("JWT is expired")
	}

	return claims, nil

}

func (j *jwtService) ValidateForgetPasswordToken(usertoken string) (*models.ForgetPasswordJwtClaim, error) {
	jwtWrapper := models.GetJWTWrapper()
	token, err := jwt.ParseWithClaims(
		usertoken,
		&models.ForgetPasswordJwtClaim{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(jwtWrapper.SecretKey), nil
		},
	)

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*models.ForgetPasswordJwtClaim)
	if !ok {
		return nil, fmt.Errorf("Couldn't parse claims")
	}

	if claims.ExpiresAt < time.Now().Local().Unix() {
		return nil, fmt.Errorf("JWT is expired")
	}

	return claims, nil

}

func GetToken(token string) *models.JWTToken {
	return &models.JWTToken{
		Token: token,
	}
}
